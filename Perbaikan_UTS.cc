#include <iostream>
using namespace std;

int main() {
    int bilangan1, bilangan2;

    cout << "Masukan Bilangan ke 1 : ";
    cin >> bilangan1;

    cout << "Masukan Bilangan ke 2 : ";
    cin >> bilangan2;

    if (bilangan1 < bilangan2) {
        cout << "Bilangan ke 1 < Bilangan ke 2 : " << bilangan1 << " < " << bilangan2 << endl;
    } else if (bilangan1 > bilangan2) {
        cout << "Bilangan ke 1 > Bilangan ke 2 : " << bilangan1 << " > " << bilangan2 << endl;
    } else {
        cout << "Bilangan ke 1 = Bilangan ke 2 : " << bilangan1 << " = " << bilangan2 << endl;
    }

    return 0;
}
